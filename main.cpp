#include <iostream>
#include <string>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

unsigned int width = 200;
unsigned int length = 20;
float bal_radius = 10;
float bal_speed_x = 0;
float bal_speed_y = 1;

unsigned int fps = 400;

sf::RectangleShape plank_body(sf::Vector2f(width, 1));
sf::RectangleShape plank_left(sf::Vector2f(1, length - 1));
sf::RectangleShape plank_rigth(sf::Vector2f(1, length - 1));

sf::CircleShape bal(bal_radius);
//bal.setPosition(0, 900);
//sf::RectangleShape row_1[16];
//for (int i = 0; i < 16; i = i + 1)
//{
//	row_1[i].setSize(sf::Vector2f(50, 50));
//	std::cout << "HOI" << std::endl;
//}


//plank_body.setPosition(960 - width / 2, 1080 - length);
//plank_body.setPosition(22,22);


//bool row_1_bool[16] = {true};
//bool row_2_bool[16] = {true};
//bool row_3_bool[16] = {true};
//bool row_4_bool[16] = {true};

void setPlankSize()
{

}

void movePlank(int x, int y)
{
	plank_body.move(x, y);
	plank_left.move(x, y);
	plank_rigth.move(x, y);
}

void drawPlank(sf::RenderWindow &window)
{
	window.draw(plank_body);
	window.draw(plank_left);
	window.draw(plank_rigth);
}

void setPlankPosition(int x)
{
	x = x - width / 2;

	plank_body.setPosition(x, plank_body.getPosition().y);
	plank_left.setPosition(x, plank_left.getPosition().y);
	plank_rigth.setPosition(x + width -1, plank_rigth.getPosition().y);
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(1920, 1080), "myPong");
	window.setFramerateLimit(fps);
	window.setVerticalSyncEnabled(false);

	sf::View view;
	view.reset(sf::FloatRect(0, 0, 1920, 1080));
	view.setViewport(sf::FloatRect(0, 0, 1.f, 1.f));
	window.setView(view);	

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;


	plank_body.setPosition(22, 1080 - length);
	plank_left.setPosition(22, 1080 - length + 1);
	plank_rigth.setPosition(22, 1080 - length + 1);


	bal.setPosition(0, 900);

	sf::RectangleShape row_1[16];
	for (int i = 0; i < 16; i++)
	{
		int z = i * 120;
		row_1[i].setSize(sf::Vector2f(50, 50));
		row_1[i].setPosition(z ,50);
	}

	sf::RectangleShape row_2[16];
	for (int i = 0; i < 16; i++)
	{
		int z = i * 120; 
		row_2[i].setSize(sf::Vector2f(50, 50));
		row_2[i].setPosition(z, 150);
	}

	sf::RectangleShape row_3[16];
	for (int i = 0; i < 16; i++)
	{
		int z = i * 120;
		row_3[i].setSize(sf::Vector2f(50, 50));
		row_3[i].setPosition(z ,250);
	}

	sf::RectangleShape row_4[16];
	for (int i = 0; i < 16; i++)
	{
		int z = i * 120; 
		row_4[i].setSize(sf::Vector2f(50, 50));
		row_4[i].setPosition(z, 350);
	}

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
			{
				window.close();
			}
       	}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			std::cout << "A key is pressed." << std::endl;
			movePlank(-1,0);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			std::cout << "A key is pressed." << std::endl;
			movePlank(1,0);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			std::cout << "A key is pressed." << std::endl;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			std::cout << "A key is pressed." << std::endl;
		}

		/* Collison - walls*/
		if (bal.getPosition().x > 1920 - bal_radius * 2)
		{
			bal_speed_x = bal_speed_x * -1;
		}
		if (bal.getPosition().x < 0)
		{
			bal_speed_x = bal_speed_x * -1;
		}
		if (bal.getPosition().y > 1080 - bal_radius * 2)
		{
			bal_speed_y = bal_speed_y * -1;
		}
		if (bal.getPosition().y < 0)
		{
			bal_speed_y = bal_speed_y * -1;
		}

		/* Collision - plank */
		if (plank_body.getGlobalBounds().intersects(bal.getGlobalBounds()))
		{
			// get position x, change x and y speed for direction
			bal_speed_y = bal_speed_y * -1;
		}

		/* Collision - blocks */
		for (int i = 0; i < 16; i++)
		{
			if (bal.getGlobalBounds().intersects(row_1[i].getGlobalBounds()))
			{
				if ((bal.getPosition().y > 50) && (bal.getPosition().y < 100))
				{
					bal_speed_x = bal_speed_x * -1;
					row_1[i].setPosition(-50, -50);
				}
				else //if ((bal.getPosition().y < 50) && (bal.getPosition().y > 100))
				{
					bal_speed_y = bal_speed_y * -1;
					row_1[i].setPosition(-50, -50);
				}
			}
		}
		for (int i = 0; i < 16; i++)
		{
			if (bal.getGlobalBounds().intersects(row_2[i].getGlobalBounds()))
			{
				if ((bal.getPosition().y > 150) && (bal.getPosition().y < 200))
				{
					bal_speed_x = bal_speed_x * -1;
					row_2[i].setPosition(-50, -50);
				}
				else //if ((bal.getPosition().y < 50) && (bal.getPosition().y > 100))
				{
					bal_speed_y = bal_speed_y * -1;
					row_2[i].setPosition(-50, -50);
				}
			}
		}
		for (int i = 0; i < 16; i++)
		{
			if (bal.getGlobalBounds().intersects(row_3[i].getGlobalBounds()))
			{
				if ((bal.getPosition().y > 250) && (bal.getPosition().y < 300))
				{
					bal_speed_x = bal_speed_x * -1;
					row_3[i].setPosition(-50, -50);
				}
				else //if ((bal.getPosition().y < 50) && (bal.getPosition().y > 100))
				{
					bal_speed_y = bal_speed_y * -1;
					row_3[i].setPosition(-50, -50);
				}
			}
		}
		for (int i = 0; i < 16; i++)
		{
			if (bal.getGlobalBounds().intersects(row_4[i].getGlobalBounds()))
			{
				if ((bal.getPosition().y > 350) && (bal.getPosition().y < 400))
				{
					bal_speed_x = bal_speed_x * -1;
					row_4[i].setPosition(-50, -50);
				}
				else //if ((bal.getPosition().y < 50) && (bal.getPosition().y > 100))
				{
					bal_speed_y = bal_speed_y * -1;
					row_4[i].setPosition(-50, -50);
				}
			}
		}

		bal.move(bal_speed_x, bal_speed_y);

		/* Mouse */
		setPlankPosition(sf::Mouse::getPosition().x);

		/* Draw */
		window.clear();

		window.setView(view);

		drawPlank(window);
		window.draw(bal);
		for (int i = 0; i < 16; i++)
		{
			window.draw(row_1[i]);
			window.draw(row_2[i]);
			window.draw(row_3[i]);
			window.draw(row_4[i]);
		}
		window.display();

//		std::cout << sf::Mouse::getPosition().x << std::endl;
	}

	return 0;
}

